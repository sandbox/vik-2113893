<?php

/**
 * @file
 * Describes plugin for test block content type.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Test block'),
  'description' => t('Test block.'),
  'single' => TRUE,
  'render callback' => 'adyax_test_content_type_render',
  'category' => array(t('Adyax test'), -9),
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param array $subtype
 *   The subtype.
 * @param array $conf
 *   Configuration as done at admin time.
 * @param array $args
 *   The array of arguments.
 * @param array $context
 *   Context - in this case we don't have any.
 *
 * @return object
 *   An object with at least title and content members.
 */
function adyax_test_content_type_render($subtype, $conf, $args, $context) {

  $nodes = db_select('node', 'n')
    ->condition('n.status', 1)
    ->fields('n', array('nid'))
    ->range(0, 3)
    ->execute()
    ->fetchCol();

  $block = new stdClass();
  $block->content = '';
  $block->title = 'Adyax Test';

  if (!empty($nodes)) {
    foreach ($nodes as $key => $value) {
      $block->content[] = node_view(node_load($value), 'teaser');
    }
  }

  return $block;
}
